import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import 'babel-polyfill';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import Store from './redux/Store';
import routes from '../src/routes';

ReactDOM.render(
    <Provider store={Store} key='provider'>
        <Suspense fallback={() => <div>...Loading</div>}>
            <Router history={browserHistory} routes={routes} />
        </Suspense>
    </Provider>,
    document.getElementById('root')
);

