import { applyMiddleware, combineReducers, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import contextReducer from './ContextReducer';
import rootReducer from '../../src/reducers';
import rootSagas from '../../src/sagas';
import { getApplicationContext } from './ContextLoader';

const initialState = { context: getApplicationContext() };
const sagaMiddleware = createSagaMiddleware();
const bootReducers = {
    context: contextReducer,
    ...rootReducer
};

const Store = createStore(
    combineReducers(bootReducers),
    initialState,
    applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(rootSagas);

export default Store;
