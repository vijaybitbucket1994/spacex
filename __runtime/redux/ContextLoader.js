import _get from 'lodash/get';
import _merge from 'lodash/merge';
import RestClient from '../../src/interfaces/RestClient/RestClient';
import settings from '../../src/settings.json'

export const DEVICE_TYPES = {
    DESKTOP: 'desktop',
    MOBILE: 'mobile',
};

export const getDevice = () => {
    const userAgent = _get(navigator, 'userAgent', '');
    let device = DEVICE_TYPES.DESKTOP;

    if (userAgent.toLowerCase().indexOf('mobi') !== -1) {
        device = DEVICE_TYPES.MOBILE;
    }
    return device;
};

const mergeCommonToDevice = () => {
    const deviceType = getDevice();
    const context = {};

    Object.keys(settings).forEach((type) => {
        context[type] = _merge({}, settings[type].common, settings[type][deviceType]);
    });
    return context;
}

const setEnvContext = (context) => {
    const deviceType = getDevice();
    const env = _get(context, `environments.env`, {});
    const envObj = _get(context, `environments.${env}`, {});
    const envContext = {
        host: location.host,
        isSecure: location.protocol === 'https:',
        protocol: location.protocol,
        ...envObj
    };

    RestClient.setContext(envContext);
    context.environment = envContext;
    context.deviceType = {
        isDesktop: deviceType === DEVICE_TYPES.DESKTOP,
        isMobile: deviceType === DEVICE_TYPES.MOBILE,
    }
    delete context.environments;
};

export const getApplicationContext = () => {
    const context = mergeCommonToDevice();
    setEnvContext(context);

    return context;
};
