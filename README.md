
SpaceX mission details
## Initial Setup

npm install

## Building & staring the application in DEV mode.

npm run start-dev

Application by default launch in https://localhost:3000/

## Building the application in PRODUCTION mode.

npm run build-prod

## Envionment Settings

Change the attribute "env": "qa" (qa/dev/local) in the reactapp/src/settings.json file.

## Sanitycheck for all the source before code commit

npm run sanitycheck

## Running Test Cases

npm run test

## Running Test Cases with Coverage Report

npm run test:coverage

Report will be generated in the reactapp/coverage/lcov-report/index.html

