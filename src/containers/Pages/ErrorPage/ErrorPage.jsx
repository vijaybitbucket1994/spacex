import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router';
import _get from 'lodash/get';
import Loader from '../../../components/site/Loader/Loader';
import Layout from '../../../components/site/Layout/Layout';
import './ErrorPage.scss';

export class ErrorPage extends Component {
    render() {
        return (
            <Layout showDashboardHeader showOutlet>
                <main className='main-content'>
                    <h3>Page Not Found</h3>
                    <Link className='homebtn' to='/mission'>
                        Home
                    </Link>
                    <Loader />
                </main>
            </Layout>
        );
    }
}

export default ErrorPage;
