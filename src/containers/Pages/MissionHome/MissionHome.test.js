import React from 'react';
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import MissionHomeConnect, { MissionHome } from './MissionHome';
const mockStore = configureMockStore();

describe('Component:MissionHome', () => {
    let wrapper;
    let store = mockStore({});
    const getMissionData = jest.fn();
    const resData = [
        {
            missionName: 'missile1',
            missionIds: 'NA',
            launchYear: '2020',
            successfullLaunch: true,
            successfullLanding: false,
            missionImageUrl: 'item.links.mission_patch,',
            flightNumber: '001'
        },
        {
            missionName: 'missile2',
            missionIds: 'NA',
            launchYear: '1999',
            successfullLaunch: true,
            successfullLanding: false,
            missionImageUrl: 'item.links.mission_patch,',
            flightNumber: '002'
        },
        {
            missionName: 'missile3',
            missionIds: 'NA',
            launchYear: '1998',
            successfullLaunch: true,
            successfullLanding: false,
            missionImageUrl: 'item.links.mission_patch,',
            flightNumber: '003'
        }
    ];

    const actions = {
        getMissionData
    };
    const props = {
        actions,
        missionData: {
            data: resData
        },
        location: {
            query: {}
        }
    };

    beforeEach(() => {
        const initialState = {};
        store = mockStore(initialState);
        // Shallow render the container passing in the mock store
        wrapper = mount(
            <Provider store={store}>
                <MissionHomeConnect actions={props.actions} missionData={props.missionData} location={props.location} />
            </Provider>
        );
    });

    test('Should load Home component', () => {
        const wrapper = shallow(
            <MissionHome actions={props.actions} missionData={props.missionData} location={props.location} />
        );
        expect(wrapper.find('.mission-home-container')).toHaveLength(1);
    });
});
