import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { handleUrlUpdate } from '../../../common/Utils/Utils';
import Layout from '../../../components/site/Layout/Layout';
import MissionCard from '../../../components/site/MissionCard/MissionCard';
import MissionFilter from '../../../components/site/MissionFilter/MissionFilter';
import { getMissionData } from '../../../actions/MissionDataAction';
import './MissionHome.scss';

export class MissionHome extends Component {
    static propTypes = {
        missionData: PropTypes.objectOf(PropTypes.object),
        getMissionData: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedYear: '',
            selectedLaunchingStatus: '',
            selectedLandingStatus: ''
        };
    }

    componentDidMount() {
        if (!isEmpty(this.props.location.query)) {
            this.setState({
                selectedYear: _get(this.props, 'location.query.launch_year', ''),
                selectedLandingStatus: _get(this.props, 'location.query.landing_success', ''),
                selectedLaunchingStatus: _get(this.props, 'location.query.launching_success', '')
            });
            const data = {
                selectedYear: _get(this.props, 'location.query.launch_year', ''),
                selectedLandingStatus: _get(this.props, 'location.query.landing_success', ''),
                selectedLaunchingStatus: _get(this.props, 'location.query.launching_success', '')
            };
            this.props.actions.getMissionData(data);
        } else {
            this.props.actions.getMissionData();
        }
    }

    handleFilter = (type, val) => {
        if(val === 'reset'){
            this.props.actions.getMissionData();
            this.setState({
                selectedYear: '',
                selectedLaunchingStatus: '',
                selectedLandingStatus: ''
            })
            this.props.router.push(`/`);
        }
        else{
            this.setState({
                [type]: val
            });
            const data = {
                ...this.state,
                [type]: val
            };
            this.props.actions.getMissionData(data);
    
            const url = handleUrlUpdate(data);
            this.props.router.push(`/mission${url}`);
        }


    };

    renderMissionCards = () => {
        const { missionData } = this.props;
        if ((this.props.missionData.data || []).length > 0) {
            return _get(missionData, 'data', []).map(item => (
                <MissionCard
                    missionName={item.missionName}
                    missionIds={item.missionIds}
                    launchYear={item.launchYear}
                    successfullLaunch={item.successfullLaunch}
                    successfullLanding={item.successfullLanding}
                    missionImageUrl={item.missionImageUrl}
                    flightNumber={item.flightNumber}
                />
            ));
        }
        return (
            <div className='default-block'>
                <h3 className='default-content'>No Data Found</h3>
                <h5>Try with Different Filter</h5>
            </div>
        );
    };

    render() {
        const { selectedYear, selectedLaunchingStatus, selectedLandingStatus } = this.state;
        return (
            <Layout>
                <main className='mission-home-container'>
                    <div className='mission-filter-block'>
                        <MissionFilter
                            handleFilter={this.handleFilter}
                            selectedYear={selectedYear}
                            selectedLaunchingStatus={selectedLaunchingStatus}
                            selectedLandingStatus={selectedLandingStatus}
                        />
                    </div>
                    <div className='mission-card-block'>{this.renderMissionCards()}</div>
                </main>
            </Layout>
        );
    }
}

const mapStateToProps = state => {
    return {
        missionData: _get(state, 'missionData', {})
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(
        {
            getMissionData
        },
        dispatch
    )
});

export default connect(mapStateToProps, mapDispatchToProps)(MissionHome);
