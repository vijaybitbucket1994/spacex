import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router';
import { URL_PATHS } from './common/Constants';
import Loader from './components/site/Loader/Loader';
const ErrorPage = lazy(() => import(/* webpackChunkName: "ErrorPage" */ './containers/Pages/ErrorPage/ErrorPage'));
const MissonHomePage = lazy(() =>
    import(/* webpackChunkName: "MissionHomePage" */ './containers/Pages/MissionHome/MissionHome')
);

const routes = (
    <Suspense fallback={() => <Loader />}>
        <Route>
            <Route path='/' component={MissonHomePage} />
            <Route path='/mission' component={MissonHomePage} />
            <Route path={`${URL_PATHS.MISSION_HOME}`} component={MissonHomePage} />
            {/* Error Route 404 Page. */}
            <Route path='*' component={ErrorPage} />
        </Route>
    </Suspense>
);

export default routes;
