import RestClient from '../RestClient/RestClient';
import RestConfig from '../RestClient/RestConfig';

export const handleUrlUpdate = data => {
    // TODO code clean up required
    let url = '';
    if (data.selectedLaunchingStatus !== '' && data.selectedLandingStatus !== '' && data.selectedYear !== '') {
        url += `&launch_success=${data.selectedLaunchingStatus}&land_success=${data.selectedLandingStatus}&launch_year=${data.selectedYear}`;
    } else if (data.selectedLaunchingStatus !== '' && data.selectedLandingStatus !== '') {
        url += `&launch_success=${data.selectedLaunchingStatus}&land_success=${data.selectedLandingStatus}`;
    } else if (data.selectedLaunchingStatus !== '' && data.selectedYear !== '') {
        url += `&launch_success=${data.selectedLaunchingStatus}&launch_year=${data.selectedYear}`;
    } else if (data.selectedLandingStatus !== '' && data.selectedYear !== '') {
        url += `&land_success=${data.selectedLandingStatus}&launch_year=${data.selectedYear}`;
    } else if (data.selectedLandingStatus !== '') {
        url += `&land_success=${data.selectedLandingStatus}`;
    } else if (data.selectedLaunchingStatus !== '') {
        url += `&launch_success=${data.selectedLaunchingStatus}`;
    } else {
        url += `&launch_year=${data.selectedYear}`;
    }
    return url;
};
export default class MissionDataApi {
    static getMissionData(params) {
        const config = RestConfig.v3ApiConfig(params);
        if (params.payload === undefined) {
            config.url = `https://api.spaceXdata.com/v3/launches?limit=100`;
        } else {
            const urlStr = handleUrlUpdate(params.payload);
            config.url = `https://api.spaceXdata.com/v3/launches?limit=100${urlStr}`;
        }

        return RestClient.get(config).then(json => {
            return json;
        });
    }
}
