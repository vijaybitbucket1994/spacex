export const ACCESS_TOKEN = 'access_token';
export const REFRESH_TOKEN = 'refresh_token';

export const HEADERS = {
    AUTH: 'Authorization'
};

export const MISSION_API = {};
