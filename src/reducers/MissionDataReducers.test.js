import { GET_MISSION_INFO_REQUEST, GET_MISSION_DATA_SUCCESS } from '../actionTypes/MissionDataActionTypes';
import MissionDataReducer from './MissionDataReducer';

const resData = [
    {
        missionName: 'missile1',
        missionIds: 'NA',
        launchYear: '2020',
        successfullLaunch: true,
        successfullLanding: false,
        missionImageUrl: 'testUrl',
        flightNumber: '001'
    },
    {
        missionName: 'missile2',
        missionIds: 'NA',
        launchYear: '1999',
        successfullLaunch: true,
        successfullLanding: false,
        missionImageUrl: 'testUrl',
        flightNumber: '002'
    },
    {
        missionName: 'missile3',
        missionIds: 'NA',
        launchYear: '1998',
        successfullLaunch: true,
        successfullLanding: false,
        missionImageUrl: 'testUrl',
        flightNumber: '003'
    }
];

describe('Reducer:MissionDataReducer', () => {
    test('should return intital state', () => {
        const reducer = MissionDataReducer(
            {},
            {
                type: GET_MISSION_INFO_REQUEST
            }
        );
        expect(reducer).toEqual({});
    });

    // test('should return success state', () => {
    //     const reducer = MissionDataReducer(
    //         {},
    //         {
    //             type: GET_MISSION_DATA_SUCCESS,
    //             data: { data:resData  }
    //         }
    //     );
    //     expect(reducer).toEqual({ ...resData });
    // });

    test('should return deafult state', () => {
        const reducer = MissionDataReducer();
        expect(reducer).toEqual({});
    });
});
