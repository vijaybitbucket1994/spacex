import MissionDatatReducer from './MissionDataReducer';
import LoaderReducer from './LoaderReducer';

const rootReducer = {
    isLoading: LoaderReducer,
    missionData: MissionDatatReducer
};

export default rootReducer;
