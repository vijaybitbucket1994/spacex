import { GET_MISSION_INFO_REQUEST, GET_MISSION_DATA_SUCCESS } from '../actionTypes/MissionDataActionTypes';
import MissionService from '../service/MissionService';

const initialState = {};
export default function MissionDataReducer(state = initialState, action = {}) {
    switch (action.type) {
        case GET_MISSION_INFO_REQUEST:
            return {
                ...initialState
            };
        case GET_MISSION_DATA_SUCCESS: {
            const filteredData = MissionService(action.data.data);
            return {
                ...initialState,
                data: filteredData
            };
        }

        default:
            return state;
    }
}
