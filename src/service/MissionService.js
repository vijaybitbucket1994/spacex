import _get from 'lodash/get';

const MissionService = data => {
    return data.map(item => ({
        missionName: item.mission_name,
        missionIds: item.mission_id,
        launchYear: item.launch_year,
        successfullLaunch: item.launch_success,
        successfullLanding: _get(item, 'rocket.first_stage.cores[0].land_success', null) || false,
        missionImageUrl: _get(item.links, 'mission_patch', ''),
        flightNumber: item.flight_number
    }));
};

export default MissionService;
