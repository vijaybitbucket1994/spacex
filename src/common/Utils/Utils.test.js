import { handleUrlUpdate } from './Utils';

describe('Utils', () => {
    test('Should call handleUrlUpdate', () => {
        const data = {
            selectedYear: '2020',
            selectedLaunchingStatus: 'true',
            selectedLandingStatus: 'true'
        };
        const option = handleUrlUpdate(data);
        expect(option).toBe('?launching_success=true&landing_success=true&launch_year=2020');
    });
});
