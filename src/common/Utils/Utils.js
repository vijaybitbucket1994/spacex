export const handleUrlUpdate = data => {
    // TODO code clean up required
    let url = '';
    if (data.selectedLaunchingStatus !== '' && data.selectedLandingStatus !== '' && data.selectedYear !== '') {
        url += `?launching_success=${data.selectedLaunchingStatus}&landing_success=${data.selectedLandingStatus}&launch_year=${data.selectedYear}`;
    } else if (data.selectedLaunchingStatus !== '' && data.selectedLandingStatus !== '') {
        url += `?launching_success=${data.selectedLaunchingStatus}&landing_success=${data.selectedLandingStatus}`;
    } else if (data.selectedLaunchingStatus !== '' && data.selectedYear !== '') {
        url += `?launching_success=${data.selectedLaunchingStatus}&launch_year=${data.selectedYear}`;
    } else if (data.selectedLandingStatus !== '' && data.selectedYear !== '') {
        url += `?landing_success=${data.selectedLandingStatus}&launch_year=${data.selectedYear}`;
    } else if (data.selectedLandingStatus !== '') {
        url +=
            url === ''
                ? `?landing_success=${data.selectedLandingStatus}`
                : `&landing_success=${data.selectedLandingStatus}`;
    } else if (data.selectedLaunchingStatus !== '') {
        url +=
            url === ''
                ? `?launching_success=${data.selectedLaunchingStatus}`
                : `&launching_success=${data.selectedLaunchingStatus}`;
    } else {
        url += url === '' ? `?launch_year=${data.selectedYear}` : `&launch_year=${data.selectedYear}`;
    }
    return url;
};

export default handleUrlUpdate;
