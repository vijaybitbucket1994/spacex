import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames/bind';
import Loader from '../Loader/Loader';
import * as styles from './Layout.scss';
import ScrollToTop from '../../core/ScrollToTop/ScrollToTop';

const cx = classNames.bind(styles);

class Layout extends Component {
    static defaultProps = {
        isSkipLoader: false,
        theme: '',
        headerTitle: 'SpacEx Launch Program',
        authorName: 'Vijayakumar M'
    };

    static propTypes = {
        isSkipLoader: PropTypes.bool,

        theme: PropTypes.string,
        children: PropTypes.node,
        headerTitle: PropTypes.string,
        authorName: PropTypes.string
    };

    render() {
        const { authorName, headerTitle, theme } = this.props;

        return (
            <main>
                <ScrollToTop />
                <div className={cx('mainContainer')}>
                    <header>
                        <h3>{headerTitle}</h3>
                    </header>

                    <article className={cx('contentContainer', theme)}>{this.props.children}</article>
                    {/* footer content goes here */}
                    <footer>
                        <h3>Developed by:{authorName}</h3>
                    </footer>
                    {!this.props.isSkipLoader && <Loader />}
                </div>
            </main>
        );
    }
}

export default Layout;
