import React from 'react';
import { shallow } from 'enzyme';
import Layout from './Layout';

describe('Component:Layout', () => {
    let wrapper;
    const renderComponent = props =>
        shallow(
            <Layout props={props} loginFooter>
                <div />
            </Layout>
        );

    describe('Render Layout', () => {
        let props;

        beforeEach(() => {
            props = {
                footerInner: true,
                children: <p>test</p>
            };
            wrapper = renderComponent(props);
        });

        test('Should load Layout component', () => {
            expect(wrapper.find('.mainContainer')).toHaveLength(1);
        });
    });
});
