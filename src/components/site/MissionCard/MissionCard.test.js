import React from 'react';
import { shallow } from 'enzyme';
import MissionCard from './MissionCard';

describe('Component:MissionCard', () => {
    let wrapper;
    const props = {
        missionName: 'missle1',
        missionIds: ['NA'],
        launchYear: '2020',
        successfullLaunch: 'true',
        missionImageUrl: 'testUrl',
        key: '001',
        flightNumber: '001'
    };

    beforeEach(() => {
        // Shallow render the container passing in the mock store
        wrapper = shallow(
            <MissionCard
                missionName={props.missionName}
                missionIds={props.missionIds}
                launchYear={props.launchYear}
                successfullLaunch={props.successfullLaunch}
                missionImageUrl={props.missionImageUrl}
                key={props.key}
                flightNumber={props.flightNumber}
            />
        );
    });

    test('Should load Home component', () => {
        const wrapper = shallow(
            <MissionCard
                missionName={props.missionName}
                missionIds={props.missionIds}
                launchYear={props.launchYear}
                successfullLaunch={props.successfullLaunch}
                missionImageUrl={props.missionImageUrl}
                key={props.key}
                flightNumber={props.flightNumber}
            />
        );
        expect(wrapper.find('.mission-card-container')).toHaveLength(1);
    });
});
