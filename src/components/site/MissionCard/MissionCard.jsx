import PropTypes from 'prop-types';
import React, { Component } from 'react';
import _get from 'lodash/get';
import './MissionCard.scss';

class MissionCard extends Component {
    static propTypes = {
        missionName: PropTypes.string,
        missionIds: PropTypes.oneOfType([PropTypes.array]),
        launchYear: PropTypes.string,
        successfullLaunch: PropTypes.bool,
        successfullLanding: PropTypes.bool,
        missionImageUrl: PropTypes.string,
        key: PropTypes.string,
        flightNumber: PropTypes.string
    };

    static defaultProps = {
        missionName: '',
        missionIds: [],
        launchYear: '',
        successfullLaunch: false,
        successfullLanding: false,
        missionImageUrl: '',
        key: '',
        flightNumber: ''
    };

    renderMisisonIds = () => {
        return (
            (this.props.missionIds || []).length > 0 &&
            this.props.missionIds.map(item => (
                <li className='mission-ids-list-item' key={item}>
                    {item}
                </li>
            ))
        );
    };

    render() {
        const {
            missionName,
            missionImageUrl,
            flightNumber,
            successfullLanding,
            successfullLaunch,
            launchYear,
            key
        } = this.props;
        return (
            <section key={key} className='mission-card-container'>
                <div className='card-img-block'>
                    <img src={missionImageUrl} alt='mission-img' className='card-img' />
                </div>
                <div className='card-details'>
                    <h5 className='mission-name' title={missionName}>
                        {`${missionName}#${flightNumber}`}
                    </h5>
                    <div className='mission-ids'>
                        <h5 className='label'>MisionIds:</h5>
                        {_get(this.props, 'missionIds', []).length > 0 ? (
                            <ul className='mission-ids-list value'>{this.renderMisisonIds()}</ul>
                        ) : (
                            <span className='value' key='list'>
                                NA
                            </span>
                        )}
                    </div>

                    <h5 className='mission-launch label'>
                        Launch year:<span className='value'>{launchYear}</span>
                    </h5>
                    <h5 className='launch-status label'>
                        Successful Launch: <span className='status value'>{`${successfullLaunch}`}</span>
                    </h5>

                    <h5 className='land-status label'>
                        Successful Landing:<span className='status value'>{`${successfullLanding}`}</span>
                    </h5>
                </div>
            </section>
        );
    }
}

export default MissionCard;
