import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import * as styles from './Loader.scss';

const cx = classNames.bind(styles);

export class Loader extends Component {
    static propTypes = {
        automationId: PropTypes.string,
        isLoading: PropTypes.bool,
        isSkipOverlay: PropTypes.bool,
        loaderClass: PropTypes.string,
        themeConfig: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    };

    static defaultProps = {
        automationId: 'at-loader',
        isLoading: false,
        isSkipOverlay: false,
        loaderClass: '',
        themeConfig: {}
    };

    shouldComponentUpdate(nextProps) {
        return this.props.isLoading !== nextProps.isLoading;
    }

    render() {
        const { automationId, isSkipOverlay, loaderClass, themeConfig } = this.props;
        return this.props.isLoading ? (
            <div className={cx('loader', loaderClass, !isSkipOverlay && 'loader-overlay')}>
                <div className={cx('loaderImage', themeConfig.wrapper)}>
                    <div automation-id={automationId} className={cx('loadingCircle', themeConfig.loader)} />
                </div>
            </div>
        ) : null;
    }
}

const mapStateToProps = state => ({
    isLoading: state.isLoading
});

export default connect(mapStateToProps)(Loader);
