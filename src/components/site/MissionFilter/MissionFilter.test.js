import React from 'react';
import { shallow } from 'enzyme';
import MissionFilter from './MissionFilter';

describe('Component:MissionFilter', () => {
    let wrapper;
    const handleFilter = jest.fn();
    const props = {
        selectedYear: '2006',
        selectedLaunchingStatus: true,
        selectedLandingStatus: true,
        handleFilter
    };

    beforeEach(() => {
        // Shallow render the container passing in the mock store
        wrapper = shallow(
            <MissionFilter
                selectedYear={props.selectedYear}
                selectedLandingStatus={props.selectedLandingStatus}
                selectedLaunchingStatus={props.selectedLaunchingStatus}
            />
        );
    });

    test('Should load Home component', () => {
        expect(wrapper.find('.mission-filter-container')).toHaveLength(1);
    });
});
