import PropTypes from 'prop-types';
import React, { Component } from 'react';
import './MissionFilter.scss';

class MissionFilter extends Component {
    static propTypes = {
        selectedYear: PropTypes.string,
        selectedLaunchingStatus: PropTypes.string,
        selectedLandingStatus: PropTypes.string,
        handleFilter: PropTypes.func
    };

    static defaultProps = {
        selectedYear: '',
        selectedLaunchingStatus: '',
        selectedLandingStatus: ''
    };

    renderMissionYears = () => {
        const years = [];
        for (let i = 2006; i <= 2020; i += 1) {
            years.push(i);
        }

        return years.map(item => (
            <li
                role='presentation'
                onClick={() => this.handleClick('selectedYear', `${item}`)}
                className={` filter-btn filter-year-list-item ${this.props.selectedYear === `${item}` ? 'active' : ''}`}
            >
                {item}
            </li>
        ));
    };

    handleClick = (type, val) => {
        this.props.handleFilter(type, val);
    };

    render() {
        const { selectedLaunchingStatus, selectedLandingStatus } = this.props;
        return (
            <section className='mission-filter-container'>
                <h3 className='filter-heading'>Filters</h3>
                <div className='launch-years'>
                    <h3 className='sec-heading'>Launch year</h3>
                    <ul className='filter-year-list'>{this.renderMissionYears()}</ul>
                </div>
                <div className='launch-block'>
                    <h3 className='sec-heading'>Successfull Launch</h3>
                    <ul className='launch-list'>
                        <li
                            role='presentation'
                            onClick={() => this.handleClick('selectedLaunchingStatus', 'true')}
                            className={`launch-list-item filter-btn ${
                                selectedLaunchingStatus === 'true' ? 'active' : ''
                            }`}
                        >
                            True
                        </li>
                        <li
                            role='presentation'
                            onClick={() => this.handleClick('selectedLaunchingStatus', 'false')}
                            className={`launch-list-item  filter-btn ${
                                selectedLaunchingStatus === 'false' ? 'active' : ''
                            }`}
                        >
                            False
                        </li>
                    </ul>
                </div>
                <div className='land-block'>
                    <h3 className='sec-heading'>Successfull Landing</h3>
                    <ul className='land-list'>
                        <li
                            role='presentation'
                            onClick={() => this.handleClick('selectedLandingStatus', 'true')}
                            className={`land-list-item filter-btn ${selectedLandingStatus === 'true' ? 'active' : ''}`}
                        >
                            True
                        </li>
                        <li
                            role='presentation'
                            onClick={() => this.handleClick('selectedLandingStatus', 'false')}
                            className={`land-list-item filter-btn ${selectedLandingStatus === 'false' ? 'active' : ''}`}
                        >
                            False
                        </li>
                    </ul>
                </div>

                <div className='land-block'>
                    <h3 className='sec-heading'>All</h3>
                    <ul className='land-list'>
                        <li
                            role='presentation'
                            onClick={() => this.handleClick('', 'reset')}
                            className={`land-list-item filter-btn ${selectedLandingStatus === 'true' ? 'active' : ''}`}
                        >
                            Reset
                        </li>
                    </ul>
                </div>
            </section>
        );
    }
}

export default MissionFilter;
