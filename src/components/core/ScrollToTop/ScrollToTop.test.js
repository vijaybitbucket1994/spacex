import React from 'react';
import { mount } from 'enzyme';
import ScrollToTop from './ScrollToTop';

describe.only('Component: ScrollToTop', () => {
    let wrapper;
    const renderComponent = () => mount(<ScrollToTop />);

    describe('Render ScrollToTop', () => {
        beforeEach(() => {
            wrapper = renderComponent();
        });

        test('Should load ScrollToTop component', () => {
            expect(wrapper.exists()).toBe(true);
        });
    });
});
