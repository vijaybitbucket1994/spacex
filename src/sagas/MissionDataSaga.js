import { put, takeLatest, call } from 'redux-saga/effects';
import MissionDataApi from '../interfaces/MissionData/MissionDataApi';
import {
    GET_MISSION_INFO_REQUEST,
    GET_MISSION_DATA_ERRORS,
    GET_MISSION_DATA_SUCCESS
} from '../actionTypes/MissionDataActionTypes';
import { SHOW_LOADER, REMOVE_LOADER } from '../actionTypes/LoaderActionTypes';

export function* getMissionDataSaga(action) {
    try {
        yield put({ type: SHOW_LOADER });
        const result = yield call(MissionDataApi.getMissionData, action);
        if (result.isSuccess) {
            yield put({ type: REMOVE_LOADER });
            yield put({
                type: GET_MISSION_DATA_SUCCESS,
                data: { data: result.data }
            });
        } else {
            yield put({ type: REMOVE_LOADER });
            yield put({ type: GET_MISSION_DATA_ERRORS });
        }
    } catch (err) {
        yield put({ type: REMOVE_LOADER });
        yield put({ type: GET_MISSION_DATA_ERRORS });
    }
}

export default function* watchAccountDataRequest() {
    yield takeLatest(GET_MISSION_INFO_REQUEST, getMissionDataSaga);
    // yield takeLatest(RECEIVE_EMAIL_REQUEST, updatePreferencesSaga);
}
