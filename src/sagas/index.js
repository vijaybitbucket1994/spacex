import { fork } from 'redux-saga/effects';
import watchMissionDataSaga from './MissionDataSaga';

export default function* rootSaga() {
    yield* [fork(watchMissionDataSaga)];
}
