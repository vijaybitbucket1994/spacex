import { GET_MISSION_INFO_REQUEST } from '../actionTypes/MissionDataActionTypes';

export const getMissionData = payload => ({
    type: GET_MISSION_INFO_REQUEST,
    payload
});

export default getMissionData;
