import { getMissionData } from './MissionDataAction';
import { GET_MISSION_INFO_REQUEST } from '../actionTypes/MissionDataActionTypes';

describe('MissionInfoRequestAction: ', () => {
    let MissionInfoRequestAction;

    test('should return the respective action type for the invoked action method', () => {
        MissionInfoRequestAction = getMissionData();
        expect(MissionInfoRequestAction.type).toBe(GET_MISSION_INFO_REQUEST);
    });
});
