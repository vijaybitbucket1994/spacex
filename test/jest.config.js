// To have consistent date time parsing both in local and CI environments we set
// the timezone of the Node process.
process.env.TZ = "GMT";

module.exports = {
  displayName: "CCHBC",
  rootDir: "../",
  setupFilesAfterEnv: ["<rootDir>/node_modules/jest-enzyme/lib/index.js"],
  testPathIgnorePatterns: ["node_modules"],
  coveragePathIgnorePatterns: [
    "<rootDir>/src/actionTypes",
    "<rootDir>/src/index.js",
    "<rootDir>/src/interfaces",
    "<rootDir>/src/reducers/index.js",
    "<rootDir>/src/routes.jsx",
    "<rootDir>/src/sagas",
    "<rootDir>/src/server.js",
    "<rootDir>/src/store.js",
    "<rootDir>/src/mock"
  ],
  bail: true,
  testEnvironment: "jsdom",
  testMatch: [
    "<rootDir>/src/**/*.test.{js,jsx}",
    "<rootDir>/steps/**/*.steps.js"
  ],
  transform: {
    "^.+\\.(js|jsx)$": "babel-jest"
  },
  // note: this config have to have in sync with moduleNameMapper in jest.config.server.js
  moduleNameMapper: {
    "\\.(css|scss)$": "jest-transform-css",
    "^.+\\.(gif|ttf|eot|svg|woff|woff2|ico|png|jpg)$": "<rootDir>/test/file.stub.js"
  },
  snapshotSerializers: ["enzyme-to-json/serializer"],
  setupFiles: ["<rootDir>/test/jest.setup.js"],
  collectCoverageFrom: [
    "<rootDir>/src/**/*.{js,jsx}"
  ],
  coverageThreshold: {
    global: {
      statements: 90,
      branches: 80,
      functions: 90,
      lines: 90
    }
  }
};
