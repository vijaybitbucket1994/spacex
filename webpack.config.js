var path = require('path');
const rootPath = path.resolve(__dirname);
const buildFolder = path.join(rootPath, './build');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const port = process.env.PORT || 3000;
const buildMode = process.env.NODE_ENV || 'development';
const isAnalyse = process.env.ANALYSE || false;
const isDev = buildMode === 'development';
const webpackPlugins = [
    new MiniCssExtractPlugin({
        filename: isDev ? 'css/[name].css' : 'css/[name].[contenthash].css'
    }),
    new CopyPlugin([
        {
            from: './src/assets',
            to: `${buildFolder}/assets`
        }
    ]),
    new CopyPlugin([
        {
            from: './__runtime/server.js',
            to: `${buildFolder}/server.js`
        }
    ]),
    new OptimizeCssAssetsPlugin({
        cssProcessor: require('cssnano'),
        cssProcessorOptions: {
            discardComments: { removeAll: true },
            reduceIdents: false,
            zindex: false
        },
        canPrint: true
    }),

    new HtmlWebpackPlugin({
        template: './src/index.html'
    })
];

// Add GZip Compress only for production build.
!isDev &&
    webpackPlugins.push(
        new CompressionPlugin({
            filename: '[path].gz[query]',
            algorithm: 'gzip',
            test: /\.(js|jsx)$|\.(css|scss)$|\.html$/,
            minRatio: 1
        })
    );

// Add only if isAnalyse is enabled.
isAnalyse &&
    webpackPlugins.push(
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            defaultSizes: 'gzip',
            openAnalyzer: isAnalyse,
            reportFilename: '../reports/bundle-report.html'
        })
    );

// Actual Building of Webpack Modules Starts Here.
module.exports = {
    mode: buildMode,
    devtool: 'source-map',
    entry: {
        app: './__runtime/index.js'
    },
    output: {
        path: buildFolder,
        filename: isDev ? 'scripts/[name].js' : 'scripts/[name].[chunkhash].js',
        chunkFilename: isDev ? 'scripts/[name].js' : 'scripts/[name].[chunkhash].js',
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.s(a|c)ss$/,
                exclude: /node_modules/,
                loader: [
                    isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: isDev,
                            modules: true,
                            isDebug: isDev,
                            minimize: !isDev,
                            localIdentName: '[local]'
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: isDev
                        }
                    },

                    'postcss-loader'
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(svg|eot|ttf|woff|woff2)$/,
                use: 'file-loader'
            }
        ]
    },
    plugins: webpackPlugins,
    devServer: {
        host: 'localhost',
        port: port,
        historyApiFallback: true,
        inline: true,
        writeToDisk: true,
        disableHostCheck: true,
        historyApiFallback: {
            disableDotRule: true
        }
    }
};
